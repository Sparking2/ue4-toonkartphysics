// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "KartPhysicsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class KARTPHYSICS_API AKartPhysicsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
